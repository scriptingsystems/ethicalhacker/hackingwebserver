## How to determine if my server is vulnerable against this vulnerability?

To detect if your servers has a version of Bash suffering this security flaw, you can connect to your server through SSH and execute:
```
env x='() { :;}; echo Vulnerable' bash -c /bin/true
```

If it is vulnerable, the standard output of this commando will return the word 'Vulnerable'.

Otherwise, if it is not vulnerable, the above command will return nothing. 
